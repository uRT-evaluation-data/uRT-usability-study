# µRT Usability Study

This project contains all relevant data of the µRT usability study, conducted in 2022 at Bielefeld University as part of the course "Autonomous Systems Engineering".
The study consists of four parts:

1. *ex ante*:<br>
   Participants were asked to fill in a questionnaire about demographic information and prior experience in various areas of computer science.
2. *ROS line following*:<br>
   In an initial session, an introductory lecture was given, explaining fundamental concepts of middlewares in general and specifically ROS.
   In the second session, participants were given the task to make AMiRo [1] follow a line in a simulation environment using ROS (C++ only), for which they were allowed two sessions (2 hours each) to complete.
   Afterwards, participants were asked to fill in a questionnaire to assess their experience.
3. *µRT tutorial*:<br>
   This time no introductory lectore was provided, but participants were instructed to work through a tutorial on µRT [2,3] instead and to fill in another questionnaire afterwards.
   Participants were allowed to spend two sessions (2 hours each) with this part.
4. *µRT line following*:<br>
   Another task was given to make AMiRo [1] follow a line, but this time using µRT on a real robot, and again participants were asked to rate their experience thereafter by means of yet another questionnaire.
   As with the other tasks, participants could spend two sessions (2 hours each) for this one.

The questionnaires of parts 2, 3 and 4 were based on USE [4] but supplemented by three further questions (cf. files in the [`questionnaires/`](questionnaires/) folder).

### Directories and Contents

- Copies of the questionaires (cf. [`questionnaires/`](questionnaires/) folder):<br>
  Originally, the questionnaires were conducted via a [LimeSurvey](https://www.limesurvey.org/) web application.
- Aggregated (raw) response data (cf. [`aggregated_responses.csv`](aggregated_responses.csv) file):<br>
  This tab saparated `.csv` file contains all answers of the four questionnaires in anonymized form.
  Each line represents a participant, who is identified by the numerical value in the first column.
- Evaluation output data (cf. [`evaluation/`](evaluation/) folder):<br>
  All data is availabe in the form of `.csv` files (tab separated).
  According plots are also available in form of `.svg` vector graphics.
  Notably, the [evaluation folder](evaluation/) contains three subfolders (besides the [participation folder](evaluation/participation/)), which contain result data for different subsets of participants:
  - [`anything/`](evaluation/anything): Complete data, considering all responses of all participants.
  - [`complete only/`](evaluation/complete only): Only particiapnts who provided information for all four parts are considered.
  - [`all tasks/`](evaluation/all tasks): Participants who provided information about all three tasks, but may have missed on the *ex ante* questionnaire are considered.
- [`LICENSE`](LICENSE) file:<br>
  Data may be accessed, modified and republished according to the Creative Commons Attribution 4.0 International license (CC-BY).

---

**[1]** Herbrechtsmeier, S., Korthals, T., Sch  ̈opping, T., and R  ̈uckert, U. (2016). "AMiRo: A modular customizable open-source mini robot platform". In 2016 20th International Conference on System Theory, Control and Computing (ICSTCC) (IEEE), 687–692

**[2]** Schöpping T, Kenneweg S. "µRT". Bielefeld University; 2022. DOI: [10.4119/unibi/2966336](https://doi.org/10.4119/unibi/2966336)

**[3]** https://gitlab.ub.uni-bielefeld.de/AMiRo/AMiRo-Apps/-/blob/main/docs/Tutorial.md

**[4]** Lund, A. M. (2001). "Measuring usability with the use questionnaire". Usability interface 8, 3–6
